<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 20-01-14
 * Time: 8.01.MD
 */

$path = ADMIN_ROOT_DIR."/models/".$action.".php";

if (!file_exists($path)) {
	header('Location: index.php?page=home&msg=Aksioni qe kerkoni te beni nuk eshte i mundur&msgType=danger');
} else {
	require_once $path;
}

?>