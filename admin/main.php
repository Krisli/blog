<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 20-01-14
 * Time: 8.01.MD
 */

session_start();

$isLogged = false;
$user = null;
if (isset($_SESSION['admin'])) {
	$isLogged = true;
	$user = $_SESSION['admin'];
}

if (empty($page)) {
    $page = "home";
}

?>

<html>

<head>
	<title>Blog Admin</title>

    <?php
    Utils::enqueueStyle('vendor/bootstrap.min.css');
    Utils::enqueueScript('vendor/jquery-3.4.1.min.js');
    Utils::enqueueScript('vendor/popper.js');
    Utils::enqueueScript('vendor/bootstrap.min.js');
    Utils::enqueueScript('vendor/sb-admin-2.min.js');

    Utils::enqueueStyle('vendor/sb-admin-2.min.css');
    Utils::enqueueStyle('vendor/fontawesome/css/all.min.css');

    Utils::enqueueStyle('main.css');
    ?>
</head>

<body id="page-top">

<div id="wrapper">

    <?php if ($isLogged) { ?>
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="?page=home">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="sidebar-brand-text mx-3">Blog <sup>admin</sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item <?php echo $page=='home' ? 'active': '' ?>">
            <a class="nav-link" href="?page=home">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Elements
        </div>


        <!-- SHTO MENU PER CDO VIEW -->
        <li class="nav-item <?php echo $page=='categories' ? 'active': '' ?>">
            <a class="nav-link" href="?page=categories">
                <i class="fas fa-fw fa-list-alt"></i>
                <span>Categories</span></a>
        </li>

        <li class="nav-item <?php echo $page=='articles' ? 'active': '' ?>">
            <a class="nav-link" href="?page=articles">
                <i class="fas fa-fw fa-file"></i>
                <span>Articles</span></a>
        </li>


        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->
    <?php } ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">
            <div class="alerts">
				<?php Router::displayAlert() ?>
            </div>

			<?php

			if (!$isLogged) {
				require_once ADMIN_ROOT_DIR."/views/login.php";
			} else {
				if (!empty($page)) {
					$path = ADMIN_ROOT_DIR."/views/".$page.".php";
					if (!file_exists($path)) {
						echo "<h1>Oops, page not found! 404</h1>";
					} else {
						require_once $path;
					}
				}
			}

			?>
        </div>

    </div>

</div>

</body>

</html>