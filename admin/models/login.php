<?php

session_start();

$emer = $_POST['emer'];
$pass = $_POST['pass'];

$found = false;
if ($emer == 'admin' && $pass == 'admin123') {
	$_SESSION['admin'] = $emer;
	$found = true;

	$msg = 'Miresevini administrator!';
	$type = 'success';
} else {
	$msg = 'Kredenciale te gabuara!';
	$type = 'danger';
}

if (!$found) {
	header('Location: index.php?page=login&msg=' . $msg . '&msgType=' . $type);
}
else {
	header('Location: index.php?page=home&msg=' . $msg . '&msgType=' . $type);
}


?>