<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 20-01-14
 * Time: 5.33.MD
 */

if (!defined('ADMIN_ROOT_DIR')) {
	define('ADMIN_ROOT_DIR', getcwd());
}

function getBaseUrl($atRoot=FALSE, $atCore=FALSE, $parse=FALSE)
{
	if (isset($_SERVER['HTTP_HOST'])) {
		$http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
		$hostname = $_SERVER['HTTP_HOST'];
		$dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

		$core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
		$core = $core[0];

		$tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
		$end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
		$base_url = sprintf( $tmplt, $http, $hostname, $end );
	}
	else $base_url = 'http://localhost/';

	if ($parse) {
		$base_url = parse_url($base_url);
		if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
	}

	$array = explode('/', $base_url);
	array_pop($array);
	array_pop($array);

	$base_url = implode('/', $array);

	return $base_url;
}

if (!defined('BASE_URL')) {
	define('BASE_URL', getBaseUrl());
}

$array = explode('/', ADMIN_ROOT_DIR);
array_pop($array);

$path = implode('/', $array);
if (!defined('ROOT_DIR')) {
	define('ROOT_DIR', $path);
}

require_once ROOT_DIR."/helpers/defines.php";
require_once ROOT_DIR."/helpers/router.php";
require_once ROOT_DIR."/helpers/utils.php";

$page = Router::getPage();
$action = Router::getAction();

if (!empty($action)) {
	require_once "action.php";
} else {
	require_once 'main.php';
}


?>

