<?php

if(isset($_SESSION['user']))
{ ?>
    <h1> ADD CATEGORY</h1>

    <form class="form-container" action="?action=add_category" method="post" style="margin:60px">

        <label for="emri"><b>Category name:</b></label><br>
        <input class="form-control" type="text" name="emer" id="emri" placeholder="Category name">

        <label for="pershkrimi"><b>Category description:</b></label>
        <textarea class="form-control" name="pershkrim" id="pershkrimi" rows="5" placeholder="Add something about the new category :"></textarea>

        <button type="submit" class="btn btn-outline-primary mt-3" type="submit" >Add Category</button>
    </form>

    <?php
}

else
    header('Location: index.php?page=login');


?>