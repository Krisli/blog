<?php

if (!isset($_SESSION['user'])) {
	header('Location: index.php?page=login');
}

$file = ROOT_DIR."/db/articles.json";
$categoryFile= ROOT_DIR."/db/categories.json";
$articles = array();
$articles = json_decode(file_get_contents($file));
$kategorite=array();
$kategorite=json_decode(file_get_contents($categoryFile));
$i=0;
?>

<div class="articles-container">
    <div class="articles-header">
        <span class="title">Articles</span>
    </div>
    
    <div class="articles-body">
        <?php if (!empty($articles)) { ?>

            <?php while($i<count($kategorite)){ ?>
            <h2>Kategoria: <?php echo $kategorite[$i]->emer; ?></h2>

            <?php foreach ($articles as $article) { ?>
               <?php if($article->kategori==$kategorite[$i]->emer){ ?>
                <div class="article">
                    <div class="article-img">
                        <a href="index.php?page=article&id= <?php echo $article->id; ?>">
                            <img src="<?php echo !empty($article->foto) ? $article->foto : IMAGES_PATH.'/no_image.png' ?>" >
                        </a>
                    </div>
                    <div class="article-text">
                        <p class="title">
                            <a href="index.php?page=article&id=<?php echo $article->id; ?>"><?php echo $article->emer ?></a>
                        </p>

                        <p class="description">
                            <?php echo $article->pershkrimi ?>
                        </p>
                    </div>
                    <div class="article-footer">
                        <span class="article-author">Created by: <?php echo $article->user; ?></span> | <span class="article-date"><?php echo $article->data;?></span><br><br>
                        <a class="btn btn-outline-primary" href="?action=delete_article&id=<?php echo $article->id ?>">Delete Article</a>
                    </div>
                </div>
                <?php }?>
            <?php } ?>
        <?php $i++; }  }  
                else { ?>
                <div class="text-center">
                    <h5>Nuk u gjet asnje artikull!</h5>
                    <img class="no-results" />
                </div>
        <?php } ?>
    </div>
    
</div>
