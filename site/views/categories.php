<?php


if (!isset($_SESSION['user'])) {
    header('Location: index.php?page=login');
}

$categoryFile = ROOT_DIR.'/db/categories.json';
$categories = json_decode(file_get_contents($categoryFile));

?>

<?php if (!empty($categories)) { ?>
    <table class="table table-hover table-dark" style="margin-top:15px;">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Delete</th>
        </tr>
        <?php foreach ($categories as $category) { ?>
            <tr>
                <td><a href="index.php?page=category&id=<?php echo $category->id ?>">
                        <?php echo $category->id ?>
                    </a>
                </td>
                <td><?php echo $category->emer ?></td>
                <td><?php echo $category->pershkrim ?></td>
                <td>
                    <a class="btn btn-outline-primary" href="?action=delete_category&id=<?php echo $category->id ?>">Delete Category</a>
                </td>
            </tr>
        <?php } ?>
    </table>
<?php }
else{  ?>
    <div class="alert alert-warning" role="alert">Asnje kategori ekzistente!</div>
<?php } ?>