<?php

$categoryFile = ROOT_DIR.'/db/categories.json';
$kategorite=array();
$kategorite=json_decode(file_get_contents($categoryFile));

if(isset($_SESSION['user']))
{ ?>
<h1> Shto artikull </h1>

<form class="form-container" action="?action=create_article" method="post" style="margin:60px">
	<label for="emri"><b>Article's name:</b></label>
	<input class="form-control" type="text" name="emer" id="emri" placeholder="Emer">
	<label for="pershkrimi"><b>Article's description:</b></label>
    <textarea class="form-control" name="pershkrim" id="pershkrimi" rows="5" placeholder="Shkruani dicka ne lidhje me artikullin tuaj"></textarea>

    <select class="form-control my-2" name="kategoria"> 
    	<?php foreach($kategorite as $kategori){
    		?> 
    	<option class="dropdown-item" value="<?php echo $kategori->id; ?>"><?php echo $kategori->emer; ?></option>
    	<?php }?>
  	</select>	

	
    <div class="form-group">
    <label for="foto"><b>Upload photo</b></label>
    <input type="text" class="form-control-file" name="photo" id="foto">
  	</div>
	<button type="submit" class="btn btn-outline-primary mt-3" type="submit" >Regjistro artikullin</button>
</form>

<?php 
} 

else
	header('Location: index.php?page=login');


?>