<?php
$userFile=ROOT_DIR."/db/users.json";
$users = json_decode(file_get_contents($userFile));

$userId = $_GET['id'];


foreach ($users as $user) {
    if ($user->id == $userId) { ?>


        <div class="card">
            <div class="img1">
              <img src="<?php echo !empty($user->profil) ? $user->profil : IMAGES_PATH . '/no_image.png' ?>" />
            </div>
            <?php

            $ditelindje=strtotime($user->ditelindja);
            $dataAktuale=strtotime(date("l jS \of F Y h:i:s A"));
            $diff=abs($dataAktuale-$ditelindje);
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24)
                / (30*60*60*24));

            // $days = floor(($diff - $years * 365*60*60*24 -
            //         $months*30*60*60*24)/ (60*60*24));

            ?>
            <h3>Welcome <?php echo $user->emer; ?> <?php echo $user->mbiemer; ?></h3>
            <?php if($months==0){ ?>
            <div class="img2">
               <img src="https://media1.tenor.com/images/a593c173c2d7a673ec250e1a91d018b6/tenor.gif?itemid=14659493"  />
            </div>
            <?php } ?>
            <div class="container">
                <hr>
                <p>Description</p>
                <hr>
            </div>
            <div class="title">
                <table>
                    <tr>
                        <th><a class="fa fa-key"></a> Id:</th>
                        <td><?php echo $user->id; ?></td>
                    </tr>
                    <tr>
                        <th><a class="fa fa-address-book"></a> Name:</th>
                        <td><?php echo $user->emer; ?></td>
                    </tr>
                    <tr>
                        <th><a class="fa fa-address-book-o"></a> Surname:</th>
                        <td><?php echo $user->mbiemer; ?></td>
                    </tr>
                    <tr>
                        <th><a class="fa fa-envelope"></a> Email:</th>
                        <td><?php echo $user->email; ?></td>
                    </tr>
                    <tr>
                        <th><a class="fa fa-birthday-cake"></a> Birthday:</th>
                        <td><?php echo $user->ditelindja; ?></td>
                    </tr>
                </table>

            </div>
            <div class="icons">
                <a href="https://twitter.com"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com"><i class="fa fa-instagram"></i></a>
                <a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a>
                <a href="https://www.whatsapp.com"><i class="fa fa-whatsapp"></i></a>
            </div>

        </div>




        <?php
    }
}

?>

