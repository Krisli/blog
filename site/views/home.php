<?php
if (!isset($_SESSION['user'])) {
	header('Location: index.php?page=login');
}

$file = "db/articles.json";
$articles = array();
$articles = json_decode(file_get_contents($file));
$i=0;
?>


<html>
<head>
    <title>Alitalia Airlines</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <meta name="viewport" content="width=500px, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/main.css">

    <style>
        .carousel-inner img {
            height:500px;
            width: 100%;
        }

    </style>
</head>

<body>

<div class="container">

    <div class="bd-example">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner">
				<?php for($j=count($articles)-1;$j>count($articles)-4;$j--) {
					if($i==0)
						$stringa="active";
					else
						$stringa="";
					?>
                    <div class="carousel-item <?php echo $stringa; ?>">

                        <img src="<?php echo $articles[$j]->foto; ?>" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <a href="index.php?page=article&id=<?php echo $articles[$j]->id; ?>" style="text-decoration: none; color:white;"><h2><?php echo $articles[$j]->emer.': '.$articles[$j]->pershkrimi; ?></h2></a>
                            <h8><?php echo $articles[$j]->data;?></h8>
                        </div>
                    </div>

					<?php $i++; }?>
            </div>


            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
</body>
</html>