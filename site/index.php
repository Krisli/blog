<?php

session_start();

$isLogged = false;
$user = null;
if (isset($_SESSION['user'])) {
	$isLogged = true;
	$user = $_SESSION['user'];
}

?>

<html>

<head>
	<title>Blog</title>
	<?php
	Utils::enqueueStyle('vendor/bootstrap.min.css');
	Utils::enqueueScript('vendor/jquery-3.4.1.min.js');
	Utils::enqueueScript('vendor/popper.js');
	Utils::enqueueScript('vendor/bootstrap.min.js');
	Utils::enqueueStyle('vendor/fontawesome/css/all.min.css');

	Utils::enqueueStyle('main.css');
	?>
</head>

<body>
<div class="container">
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#">Klase</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item <?php echo $page=='home' ? 'active': '' ?>">
						<a class="nav-link" href="?page=home">Home <span class="sr-only">(current)</span></a> <!--Si behet faqja me dinamike duke bashkengjitur linket...Dmth kontenti i home i shtohet kontentit te index -->
					</li>
					<?php if (!$isLogged) { ?>
						<li class="nav-item <?php echo $page=='login' ? 'active': '' ?>">
							<a class="nav-link" href="?page=login">Login</a>
						</li>

						<li class="nav-item <?php echo $page=='register' ? 'active': '' ?>">
							<a class="nav-link" href="?page=register">Register</a>
						</li>

					<?php } else { ?>
						<li class="nav-item <?php echo $page=='users' ? 'active': '' ?>">
							<a class="nav-link" href="?page=users">Users</a>
						</li>

						<li class="nav-item <?php echo $page=='create_article' ? 'active': '' ?>">
							<a class="nav-link" href="?page=create_article">Add article</a>
						</li>

						<li class="nav-item <?php echo $page=='articles' ? 'active': '' ?>">
							<a class="nav-link" href="?page=articles">Articles</a>
						</li>

						<li class="nav-item <?php echo $page=='add_category' ? 'active': '' ?>">
							<a class="nav-link" href="?page=add_category">Add category</a>
						</li>

						<li class="nav-item <?php echo $page=='categories' ? 'active': '' ?>">
							<a class="nav-link" href="?page=categories">Categories</a>
						</li>

						<li class="nav-item <?php echo $page=='logout' ? 'active': '' ?>">
							<a class="nav-link" href="?page=logout">Logout</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		</nav>
	</header>

	<div class="content">

		<div class="alerts">
			<?php Router::displayAlert() ?>
		</div>

		<?php
		if (!empty($page)) {
		    $path = ROOT_DIR."/site/views/".$page.".php";
			if (!file_exists($path)) { //psh nese login.php nuk ekziston
				echo "<h1>Oops, page not found! 404</h1>";
			} else {
				require_once $path;
			}
		}
		?>

	</div>

	<footer>
		<div class="footer">

		</div>
	</footer>

</div>
</body>

</html>