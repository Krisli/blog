<?php

$categoryFile = ROOT_DIR.'/db/categories.json';


$msg  = '';
$type = '';

$categories = json_decode(file_get_contents($categoryFile));

$categoryId = $_GET['id'];

if (empty($categoryId)) {

	$msg  = "ID e kategorise eshte bosh!";
	$type = "danger";

	header('Location: index.php?page=category&msg=' . $msg . '&msgType=' . $type);
}

for ($i = 0; $i < count($categories); $i++) 
{
	if ($categories[$i]->id == $categoryId) 
	{
		unset($categories[$i]);
		$msg  = "Kategoria me ID: $categoryId u fshi me sukses!";
		$type = 'success';
		break;
	}
}



$perfundimi = json_encode(array_values($categories));

file_put_contents($categoryFile, $perfundimi);

header('Location: index.php?page=categories&msg=' . $msg . '&msgType=' . $type);


?>