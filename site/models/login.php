<?php

$userFile = ROOT_DIR.'/db/users.json'; //sepse ndodhemi tek login.php brenda models

$fileSize = filesize($userFile);

$found = false;
$msg   = '';
$type  = '';
if (!empty($fileSize)) {
	$users = json_decode(file_get_contents($userFile)); //u krijua nje vektor objektesh ku secili user perfaqeson nje objekt

	foreach ($users as $user) {
		if ($user->emer == $_POST['emer']) {
			if ($user->pass == $_POST['pass']) {
				session_start();

				$_SESSION['user'] = $user;

				$msg   = 'Logini u krye me sukses!';
				$type  = 'success';
				$found = true;
				break;
			}
			else {
				$msg  = 'Fjalekalim i gabuar!';
				$type = 'danger';
			}

		}
		else {
			$msg  = 'Useri nuk u gjet!';
			$type = 'danger';
		}
	}
}
else {
	$msg  = 'Databaza eshte bosh!';
	$type = 'warning';
}


if (!$found) {
	header('Location: index.php?page=login&msg=' . $msg . '&msgType=' . $type);
}
else {
	header('Location: index.php?page=home&msg=' . $msg . '&msgType=' . $type);
}


?>