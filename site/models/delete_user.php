<?php

$userFile = ROOT_DIR.'/db/users.json';
$fileSize = filesize($userFile);

$msg = '';
$type = '';

$users = json_decode(file_get_contents($userFile));

$userId = $_GET['id'];

if (empty($userId)) {
	$msg = "ID e userit eshte bosh!";
	$type = "danger";

	header('Location: index.php?page=users&msg='.$msg.'&msgType='.$type);
}

for($i=0;$i<count($users);$i++)
{
	if ($users[$i]->id== $userId) {
		unset($users[$i]);
		$msg = "Useri me ID: $userId u fshi me sukses!";
		$type = 'success';
	}
}

$perfundimi=json_encode(array_values($users));
file_put_contents($userFile, $perfundimi);

header('Location: index.php?page=users&msg='.$msg.'&msgType='.$type);


?>