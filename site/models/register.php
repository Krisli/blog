<?php

$userFile = ROOT_DIR.'/db/users.json';

$user = new stdClass();
$user->id = strtotime(date('Y-m-d H:i:s'));
$user->emer = $_POST['emer'];
$user->mbiemer = $_POST['mbiemer'];
$user->email = $_POST['email'];
$user->pass = $_POST['pass'];
$user->profil=$_POST['profili'];
$user->ditelindja=$_POST['birthdate'];

$userJson = json_encode($user);
$fileSize = filesize($userFile);

$users = array();
if (!empty($fileSize)) {
	$file = file_get_contents($userFile);

	$users = json_decode($file);
}
$users[] = $user;


$usersJson = json_encode($users);

$result = file_put_contents($userFile, $usersJson);
if (!empty($result)) {
	header("Location:index.php?page=home");
} else {
	header("Location:index.php?page=register");
}

?>