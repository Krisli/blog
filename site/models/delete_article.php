<?php
session_start();

$articleFile = ROOT_DIR.'/db/articles.json';

$msg  = '';
$type = '';

$articles = json_decode(file_get_contents($articleFile));

$articleId = $_GET['id'];
$articleUser = $_SESSION['user']->emer;



if (empty($articleId)) {

    $msg  = "ID e artikullit eshte bosh!";
    $type = "danger";

    header('Location: index.php?page=articles&msg=' . $msg . '&msgType=' . $type);
}


for ($i = 0; $i < count($articles); $i++) {
    if ($articleUser == 'krisli') {
        if ($articles[$i]->id == $articleId) {
            unset($articles[$i]);
            $msg = "Artikulli me ID: $articleId u fshi me sukses!";
            $type = 'success';
            break;
        }
    }
    else{
        $msg ='Ju nuk keni akses mbi kete artikull';
        $type = 'warning';
    }
}



$perfundimi = json_encode(array_values($articles));

file_put_contents($articleFile, $perfundimi);

header('Location: index.php?page=articles&msg=' . $msg . '&msgType=' . $type);


?>
