<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 20-01-14
 * Time: 5.57.MD
 */


$path = ROOT_DIR."/site/models/".$action.".php";

if (!file_exists($path)) {
	header('Location: index.php?page=home&msg=Aksioni qe kerkoni te beni nuk eshte i mundur&msgType=danger');
} else {
	require_once $path;
}


?>