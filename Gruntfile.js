const sass = require('node-sass');

module.exports = function (grunt) {
    grunt.initConfig({

        sass: {
            options: {
                implementation: sass,
                sourceMap: true
            },
            dist: {
                files: {
                    'assets/css/main.css': 'assets/scss/main.scss'
                }
            }
        },
        watch: {
            css: {
                files: 'scss/**',
                tasks: ['compile-style']
            }
        }
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('compile-style', 'sass');
    grunt.registerTask('watch-style', 'watch');

    grunt.registerTask(
        'all',
        [
            'compile-style',
            'watch-style'
        ]
    );
};