<?php

$task = isset($_GET['task']) ? $_GET['task'] : null;

if (empty($task)) {
	$task = "GET";
} else {
	$task = strtoupper($task);
}

switch ($task) {
	case "GET":
		getCategories();
		break;

	default:
		getCategories();
}

function getCategories() {
	$catFile = ROOT_DIR.'/db/categories.json';
	$fileSize = filesize($catFile);

	if (!empty($fileSize)) {
		echo file_get_contents($catFile);
	} else {
		echo json_encode([
			"error" => true,
			"msg" => "Categories file is missing!"
		]);
	}
}