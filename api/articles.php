<?php

$task = isset($_GET['task']) ? $_GET['task'] : null;

if (empty($task)) {
	$task = "GET";
} else {
	$task = strtoupper($task);
}

switch ($task) {
	case "GET":
		getArticles();
		break;

	default:
		getArticles();
}

function getArticles() {
	$artFile = ROOT_DIR.'/db/articles.json';
	$fileSize = filesize($artFile);

	if (!empty($fileSize)) {
		echo file_get_contents($artFile);
	} else {
		echo json_encode([
			"error" => true,
			"msg" => "Articles file is missing!"
		]);
	}
}