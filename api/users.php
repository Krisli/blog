<?php

$task = isset($_GET['task']) ? $_GET['task'] : null;

if (empty($task)) {
	$task = "GET";
} else {
	$task = strtoupper($task);
}

switch ($task) {
	case "GET":
		getUsers();
		break;

	default:
		getUsers();
}

function getUsers() {
	$userFile = ROOT_DIR.'/db/users.json';
	$fileSize = filesize($userFile);

	if (!empty($fileSize)) {
		echo file_get_contents($userFile);
	} else {
		echo json_encode([
			"error" => true,
			"msg" => "User file is missing!"
		]);
	}
}