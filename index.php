<?php

function getBaseUrl($atRoot=FALSE, $atCore=FALSE, $parse=FALSE)
{
	if (isset($_SERVER['HTTP_HOST'])) {
		$http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
		$hostname = $_SERVER['HTTP_HOST'];
		$dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

		$core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
		$core = $core[0];

		$tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
		$end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
		$base_url = sprintf( $tmplt, $http, $hostname, $end );
	}
	else $base_url = 'http://localhost/';

	if ($parse) {
		$base_url = parse_url($base_url);
		if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
	}

	return $base_url;
}

if (!defined('ROOT_DIR')) {
    define('ROOT_DIR', getcwd());
}

if (!defined('BASE_URL')) {
	define('BASE_URL', getBaseUrl());
}

require_once "helpers/defines.php";
require_once "helpers/router.php";
require_once "helpers/utils.php";

$page = Router::getPage();
$action = Router::getAction();
$api = Router::getApiAction();

if (!empty($api)) {
	$path = ROOT_DIR."/api/".$api.".php";
	if (!file_exists($path)) {
		echo json_encode([
			"error" => true,
			"msg"  => "Requested endpoint is not available!"
		]);
		exit;
	} else {
		require_once $path;
	}
} else {
	if (!empty($action)) {
		require_once "site/action.php";
	} else {
		require_once 'site/index.php';
	}
}


?>