<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 20-01-14
 * Time: 5.32.MD
 */

class Router
{
	static function getPage()
	{
		$page = null;
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		}

		return $page;
	}

	static function getAction()
	{
		$action = null;
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		}

		return $action;
	}

	static function getApiAction()
	{
		$api = null;
		if (isset($_GET['api'])) {
			$api = $_GET['api'];
		}

		return $api;
	}

	static function getMessage()
	{
		$msg = null;
		if (isset($_GET['msg'])) {
			$msg = $_GET['msg'];
		} else {
			return null;
		}

		$msgType = 'success';
		if (isset($_GET['msgType'])) {
			$msgType = $_GET['msgType'];
		}

		$response          = new stdClass();
		$response->msg     = $msg;
		$response->msgType = $msgType;

		return $response;
	}

	static function displayAlert($alert = null)
	{
		if (empty($alert)) {
			$alert = self::getMessage();
		}

		$html = '';

		if (!empty($alert)) {
			$html .= "<div class='alert alert-$alert->msgType' role='alert' >";
			$html .=    $alert->msg;
			$html .= "</div>";
		}

		echo $html;
	}
}

