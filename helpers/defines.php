<?php
/**
 * Created by PhpStorm.
 * User: krisl
 * Date: 12/28/2019
 * Time: 7:51 PM
 */

if (!defined('ASSETS_PATH')) {
	define('ASSETS_PATH', BASE_URL."/assets");
}

if (!defined('IMAGES_PATH')) {
	define('IMAGES_PATH', ASSETS_PATH."/images");
}
?>