<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 20-01-19
 * Time: 3.17.MD
 */

class Utils
{
	/**
	 * Includes stylesheet
	 *
	 * @param $file string path of the css file
	 */
	static function enqueueStyle($file)
	{
		echo "<link rel='stylesheet' href='".ASSETS_PATH."/css/$file'>";
	}

	/**
	 * Includes script
	 *
	 * @param        $file string path of the js file
	 * @param string $attributes for script attributes like async and defer, default empty
	 */
	static function enqueueScript($file, $attributes = '')
	{
		echo "<script src='".ASSETS_PATH."/js/$file' $attributes></script>";
	}
}